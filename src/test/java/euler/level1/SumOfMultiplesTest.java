package euler.level1;

import static org.hamcrest.core.Is.*;
import static org.hamcrest.core.IsEqual.*;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class SumOfMultiplesTest {

	@Test
	public void sumOfMultiplesOfTwo() {
		// assemble
		SumOfMultiples multiple = new SumOfMultiples();
		int init = 0;
		int end = 9;
		int multiplier = 2;
		int expect = 20;
		// act
		int actual = multiple.sumOfMultiple(init, end, multiplier);
		// assert
		assertThat(actual, is(equalTo(expect)));
	}

	@Test
	public void sumOfMultiplesOfThree() {
		// assemble
		SumOfMultiples multiple = new SumOfMultiples();
		int init = 0;
		int end = 10;
		int expect = 18;
		int multiplier = 3;
		// act
		int actual = multiple.sumOfMultiple(init, end, multiplier);
		// assert
		assertThat(actual, is(equalTo(expect)));
	}

	@Test
	public void sumOfMultiplesOfThirteen() {
		// assemble
		SumOfMultiples multiple = new SumOfMultiples();
		int init = 0;
		int end = 9;
		int expect = 0;
		int multiplier = 13;
		// act
		int actual = multiple.sumOfMultiple(init, end, multiplier);
		// assert
		assertThat(actual, is(equalTo(expect)));
	}

	@Test
	public void sumOfTwoMultiples() {
		// assemble
		SumOfMultiples multiple = new SumOfMultiples();
		int init = 0;
		int end = 19;
		int expect = 78;
		int multiplier1 = 3;
		int multiplier2 = 5;
		// act
		int actual = multiple.sumOfTwoMultiples(init, end, multiplier1, multiplier2);
		// assert
		assertThat(actual, is(equalTo(expect)));
	}
	
	@Test
	public void sumOfTwoMultiplesUpToHundred() {
		// assemble
		SumOfMultiples multiple = new SumOfMultiples();
		int init = 0;
		int end = 100;
		int expect = 2418;
		int multiplier1 = 3;
		int multiplier2 = 5;
		// act
		int actual = multiple.sumOfTwoMultiples(init, end, multiplier1, multiplier2);
		// assert
		assertThat(actual, is(equalTo(expect)));
	}
	
	@Test
	public void sumOFTwoMultiplesUpToOneThousand() {
		// assemble
		SumOfMultiples multiple = new SumOfMultiples();
		int init = 0;
		int end = 999;
		int expected = 233168;

		// act
		int actual = multiple.sumOfTwoMultiples(init, end, 3, 5);
		// assert
		assertThat(actual, is(equalTo(expected)));
	}
	
	
}
