package euler.level1;

import static org.hamcrest.core.Is.*;
import static org.hamcrest.core.IsEqual.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.math.BigInteger;

import org.junit.Test;


public class Problem10Test {
	@Test
	public void sumOfPrimesUpTo10ResultIs17() {
		// assemble
		int limit = 10;
		BigInteger sum = BigInteger.valueOf(17);
		// act
		BigInteger actual = Problem10.sumOfPrimes(10);
		// assert
		assertEquals(sum, actual);
	}
}
