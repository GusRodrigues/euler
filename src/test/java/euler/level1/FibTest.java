
package euler.level1;

import static euler.level1.Fibonacci.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigInteger;

import org.junit.Test;

/**
 * @author Gustavo
 *
 */
public class FibTest {

	@Test
	public void testInneficientFibBaseCase1() {
		// assemble
		int n = 1;
		BigInteger expected = BigInteger.valueOf(1);
		// act
		BigInteger actual = fib(n);
		// assert
		assertEquals(actual + " must be equal to " + expected, expected, actual);

	}

	@Test
	public void testInneficientFibBaseCase2() {
		// assemble
		int n = 2;
		BigInteger expected = BigInteger.valueOf(1);
		// act
		BigInteger actual = fib(n);
		// assert
		assertEquals(actual + " must be equal to " + expected, expected, actual);
	}

	@Test
	public void testInneficientFib6thNumberReturns8() {
		// assemble
		int n = 6;
		BigInteger expected = BigInteger.valueOf(8);
		// act
		BigInteger actual = fib(n);
		// assert
		assertEquals(actual + " must be equal to " + expected, expected, actual);

	}

	@Test
	public void testInneficientFib40thNumber() {
		// assemble
		int n = 40;
		BigInteger expected = BigInteger.valueOf(102334155);
		// act
		BigInteger actual = fib(n);
		// assert
		assertEquals(actual + " must be equal to " + expected, expected, actual);
	}

	@Test
	public void testInneficientFib50thNumber() {
		// assemble
		int n = 50;
		BigInteger expected = BigInteger.valueOf(12586269025l);
		// act
		BigInteger actual = fib(n); // needs to be a bigint because... NUMBERS ARE TOO BIG!

		// assert
		assertEquals(actual + " must be equal to " + expected, expected, actual);
	}

	@Test
	public void testIfSumOfEvenWillBe44() {
		BigInteger expected = BigInteger.valueOf(44);
		int n = 10;
		// act
		BigInteger actual = sumOfEvenFibUpTo(n);
		assertTrue(expected + " must be equal to " + actual, expected.equals(actual));

	}

	@Test
	public void testIfSumOfEvenWillBe4613732() {
		BigInteger expected = BigInteger.valueOf(4613732);
		int n = 33;
		// act
		BigInteger actual = sumOfEvenFibUpTo(n);
		assertTrue(expected + " must be equal to " + actual, expected.equals(actual));

	}
	
}
