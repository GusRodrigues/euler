package euler.level1;

public class SumOfMultiples {

	/**
	 * Sum of all positive elements that are multiple of the supplied integer.
	 * 
	 * @param init       the start of the sequence. >= 0
	 * @param end        the end of the sequence >= 0
	 * @param multiplier
	 * @return The sum of all positive elements that are multiple of the supplied
	 *         {@link Integer}.
	 */
	public int sumOfMultiple(int init, int end, int multiplier) {
		// ([2*a + a(e-1)] * e)
		int e = (end - init) / multiplier;

		if (e < 1) {
			return 0;
		}
		return Integer.divideUnsigned(e * ((2 * multiplier) + ((multiplier * e) - multiplier)), 2);
	}

	/**
	 * Sum of all positive elements that are multiple of the supplied integers.
	 * 
	 * @param init        the start of the sequence. >= 0
	 * @param end         the end of the sequence >= 0
	 * @param multiplier1 first multiplier
	 * @param multiplier2 second multiplier
	 * @return The resulting sum of elements that are multiple of the supplied
	 *         {@link Integer}s.
	 */
	public int sumOfTwoMultiples(int init, int end, int multiplier1, int multiplier2) {
		int LMC = LCM(multiplier1, multiplier2);
		return sumOfMultiple(init, end, multiplier1) + sumOfMultiple(init, end, multiplier2)
				- sumOfMultiple(init, end, LMC);
	}

	/**
	 * The Least common multiple
	 * 
	 * @param multiplier1
	 * @param multiplier2
	 * @return the resulting least common multiple.
	 */
	private static int LCM(int multiplier1, int multiplier2) {
		return multiplier1 * multiplier2;
	}
}
