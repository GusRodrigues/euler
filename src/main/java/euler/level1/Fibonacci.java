package euler.level1;

import java.math.BigInteger;

/**
 * 
 * The Fibonacci class will provide methods to:
 * <ul>
 * <li>find the Nth Fibonacci number</li>
 * <li>sum all the Fibonacci number up to the NTh</li>
 * <li>sum all even fibonacci numbers</li>
 * </ul>
 * 
 * 
 * @author Gustavo
 *
 */
public class Fibonacci {	
	
	/**
	 * Even Fibonacci is given as by the F(n)
	 * F(n + 1) is even IFF F(n + 4) is even;
	 * 
	 * @param up to Nth number to sum
	 * @return {@code BigInteger} with the sum.
	 */	
	public static BigInteger sumOfEvenFibUpTo(int n) {
		BigInteger sumOfFib = BigInteger.valueOf(0);
		for (int i = 1; i <= n; i++) {
			BigInteger fib1 = fib(i + 1);
			BigInteger fib2 = fib(i + 4);

			if (fib1.remainder(BigInteger.valueOf(2)).equals(BigInteger.ZERO)
					&& fib2.remainder(BigInteger.valueOf(2)).equals(BigInteger.ZERO)) {

				sumOfFib = sumOfFib.add(fib1);

				i += 2;
			}
		}
		return sumOfFib;
	}


	/**
	 * Bottom-up apprach for a given Fibonacci Number.
	 * 
	 * @param n The desired Nth Fibonacci Number
	 * @return the {@code BigInteger} representing the Nth Fibonacci Number
	 */
	public static BigInteger fib(int n) {
		/*
		 * T(n) ::= O(n) worst case.
		 * M(n) ::= O(1) 
		 */
		BigInteger first = BigInteger.valueOf(1); // first fib num is 1
		BigInteger second = BigInteger.valueOf(1); // second fib num is 2
		BigInteger sum = BigInteger.valueOf(0);
		if (n == 0) {
			return sum; // sum is zero. prevent use of more memory.
		}
		if (n < 3) {
			return first;
		}
		for (int i = 3; i <= n; i++) {
			sum = first.add(second); // sum the first with the second
			first = second; // move the first one number
			second = sum; // second will be i+1
		}

		return sum;
	}

}
