package euler.level1;

import java.math.BigInteger;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;


public class Problem10 {
	/*
	 * The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
	 */
	
	/**
	 * Sum all prime numbers from 2 to the limit number.
	 * @param limit The upper limit.
	 * @return the sum of the prime numbers.
	 */
	public static BigInteger sumOfPrimes(int limit) {
		long sum = 0;
		if (limit <= 10) {
			int answer = smallPrime(limit);
			sum += answer;
		}
		for (int i = 3; i < limit; i+=2) {
			isPrime(i);
		}
		return BigInteger.valueOf(sum);
	}
	
	private static int smallPrime(int limit) {
		if (limit < 2) {
			return 0;
		}
		int sum = 2;
		
		for (int i = 3; i < limit; i+=2) {
			if (i == 3)
				sum += 3;
			if (i == 5)
				sum += 5;
			if (i == 7)
				sum += 7;
		}
		
		return sum;
	}
	
	private static boolean isPrime(int i) {
		// module of basePrime is true, then, false. otherwise, true.
		// primeList is ALWAYS initialized with 2, 3, 5, 7.
		List<Integer> primeList = Arrays.asList(2,3,5,7);
		
		return false;
	}
}
